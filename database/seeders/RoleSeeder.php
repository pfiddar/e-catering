<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::create([
            'name' => 'admin',
            'updated_at' => now(),
            'created_at' => now(),
        ]);

        Role::create([
            'name' => 'buyer',
            'updated_at' => now(),
            'created_at' => now(),
        ]);
    }
}
