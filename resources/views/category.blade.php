@extends('layouts.app')
@section('content')
<div class="container">
    <div>
        <button type="button" class="btn btn-success float-right mb-1" data-bs-toggle="modal" data-bs-target="#modalTambahKategori">Tambah Kategori</button>
    </div>
    <div class="row justify-content-center">
        <div class="table-responsive">
            {{ $dataTable->table() }}
        </div>
    </div>
</div>
<div class="modal fade" id="modalTambahKategori" tabindex="-1" aria-labelledby="modalTambahKategori" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kategori Produk</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="{{ route('category.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="name">Nama</label><br>
                        <input type="text" name="name" id="name"class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}"  required autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="description">Deskripsi</label><br>
                        <input type="description" name="description" id="description" class="form-control @error('description') is-invalid @enderror" required autofocus>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
{{ $dataTable->scripts() }}
@endpush