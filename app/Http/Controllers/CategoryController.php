<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\CategoryDataTable;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(CategoryDataTable $dataTable){
        return $dataTable->render('category');
    }

    public function store(Request $request){
        Category::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        return redirect()->route('category');
    }
}
